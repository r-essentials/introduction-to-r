# My little timeseries plot

# Load packages ---------------------------------------------------------------------------------------------------

library(tidyverse)
library(cowplot)

# Create my own functions -----------------------------------------------------------------------------------------

my_own_timeseries_bpm_plot_function <- function () {
    
    df_spotify_timeseries_filteres <- 
        df_spotify_timeseries |> 
        filter(released_year >= selected_release_years_first & released_year <= selected_release_years_last) |>
        mutate(
            bpm = if_else(bpm > bmp_split, "80+", "80-")
        )
    # df_spotify_timeseries_filteres
    
    ggplot(
        data = df_spotify_timeseries_filteres,
        mapping = aes(
            x = released_year,
            y = acousticness_prop
        )
    ) +
        geom_jitter(alpha = 0.9) +
        geom_boxplot(
            mapping = aes(group = released_year),
            alpha = 0.7
        ) +
        theme_minimal_hgrid(
            font_size = 10,
            font_family = "Fira Sans Condensed"
        ) +
        facet_wrap(vars(bpm)) +
        scale_x_continuous(
            breaks = c(2010:2023)
        ) +
        panel_border()
    
}

my_own_timeseries_bpm_plot_function2 <- function (
        # My first comment.
        selected_release_years_first,
        selected_release_years_last,
        bmp_split = 80
) {
    
    df_spotify_timeseries_filteres <- 
        df_spotify_timeseries |> 
        filter(released_year >= selected_release_years_first & released_year <= selected_release_years_last) |>
        mutate(
            bpm = if_else(bpm > bmp_split, "80+", "80-")
        )
    # df_spotify_timeseries_filteres
    
    ggplot(
        data = df_spotify_timeseries_filteres,
        mapping = aes(
            x = released_year,
            y = acousticness_prop
        )
    ) +
        geom_jitter(alpha = 0.9) +
        geom_boxplot(
            mapping = aes(group = released_year),
            alpha = 0.7
        ) +
        theme_minimal_hgrid(
            font_size = 10,
            font_family = "Fira Sans Condensed"
        ) +
        facet_wrap(vars(bpm)) +
        scale_x_continuous(
            breaks = c(2010:2023)
        ) +
        panel_border()
    
}

# Load my data ----------------------------------------------------------------------------------------------------

df_spotify_timeseries <- read_rds(file = "repo/df-spotify-timeseries.rds")

# Create plots ----------------------------------------------------------------------------------------------------

# Plot 1: 2009-2014
selected_release_years_first <- 2009
selected_release_years_last <- 2014
bmp_split <- 80

my_own_timeseries_bpm_plot_function()

# Plot 2: 2020

selected_release_years_first <- 2020
selected_release_years_last <- 2020
bmp_split <- 80

my_own_timeseries_bpm_plot_function()

# Plot 3: 2016-2017

selected_release_years_first <- 2016
selected_release_years_last <- 2017
bmp_split <- 80

my_own_timeseries_bpm_plot_function()

# USE A MORE ADVANCED PLOT FUNCTION STYLE

# Plot 1: 2009-2014

plot_2009 <- 
    my_own_timeseries_bpm_plot_function2(
        selected_release_years_first = 2009,
        selected_release_years_last = 2014, 
    )
plot_2009

# Plot 2: 2020

plot_2020 <- 
    my_own_timeseries_bpm_plot_function2(
        selected_release_years_first = 2020,
        selected_release_years_last = 2020, 
    )
plot_2020

# Plot 3: 2016-2017

plot_2016 <- 
    my_own_timeseries_bpm_plot_function2(
        selected_release_years_first = 2016,
        selected_release_years_last = 2017, 
    )
plot_2016

# Save our plots --------------------------------------------------------------------------------------------------

# This is wrapper around ggsave() from the cowplot package with better deafults for you:
save_plot(filename = "output/Plot 2009.png", plot = plot_2009)

save_plot(filename = "output/Plot 2020.png", plot = plot_2016)

save_plot(filename = "output/Plot 2016.png", plot = plot_2020)
