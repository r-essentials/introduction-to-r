# Setup R and R Studio

1. Download and install RStudio.
2. Download and install R.
3. Configure RStudio.
4. Download the course files (or use git if you are brave).
5. Setting up a _project_ in RStudio.
