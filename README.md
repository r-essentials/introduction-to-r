# Introduction to R for Data Analytics and Computational Social Science

The »Introduction to R for Data Analytics and Computational Social Science« offers a complete introduction to the R programming language for data science and computational social science. Taking this course students will learn how to: 

- Setting up and handling the RStudio IDE.
- Import, transform, visualize, and model data; 
- Recognize and handle common data structures; 
- Setup a reproducible data science project; 
- Communicate effectively a project’s insights;
- Automating common tasks to minimize errors and time loss.

Data literacy is increasingly required in business, technology, and academic work because data is everywhere. R is the lingua franca of data science for its powerful and easy-to-use tools for statistical analysis and data visualization. 

This course is designed for master’s students specializing in a quantitative-oriented or computational social science program. No prior experience or knowledge in data analysis and programming is required. However, students must be curious and animated by an intrinsic motivation to learn R and data science. The teaching style is hands-on and participative: instructive phases, which usually consist of live coding, alternate with individual phases that enable the participants to deepen their learning and insights. A passive »sit and listen« attitude is discouraged since this typically undermines effective and durable learning.

Both Base R approaches and modern variants of the Tidyverse will be discussed and practiced during the two block sessions. In addition to the learning and experimental application, the focus will be on the creation of a (blueprint) project that depicts the data science workflow and creates a practical and realistic setting based on real research data and problems.

## Literature

### Essentials

- Wickham and Grolemund (2017). R for Data Science. O’Reilly. https://r4ds.had.co.nz
- Locke (2017). Working with R. Locke Data
- Locke (2017). Data Manipulation in R. Locke Data

### Visualisation

- Wilke (2019). Fundamentals of Data Visualization. O'Reilly
- Healy (2019). Data Visualization. Princeton University Press
- Wickham (2010). ggplot2: Elegant Graphics for Data Analysis. Springer

### Dynamic reports

- https://quarto.org/
- https://yihui.org/knitr/
- https://pandoc.org
- https://bookdown.org/yihui/rmarkdown/

### Advanced

- Wickham (2019). Advanced R. CRC. https://adv-r.hadley.nz
- Grosser, Bumann & Wickham (2021). Advanced R Solutions. CRC
