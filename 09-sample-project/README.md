# Sample bluepring project

1. Create a RStudio project.
2. Add data, e.g. <https://www.bfs.admin.ch/bfs/en/home/statistics/education-science/pupils-students/tertiary-higher-education-institutions.html>.
3. Create sample import script.
4. Create repo folder.
5. Save sample RDS data.
6. Create sample analysis script.
7. Start discovery!